import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HelperService } from '../services/helper.service';
import { HomeService } from '../services/home.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit {
  categoryData: any = [];
  categoryForm: FormGroup;
  modalRef: BsModalRef;
  editId: any;
  constructor(
    private home: HomeService,
    private modalService: BsModalService,
    public helper: HelperService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.getCategories();
    this.categoryForm = this.fb.group({
      title: [''],
      description: [''],
    });
  }

  ngOnInit(): void {
  }

  getCategories() {
    this.home.getCategories().subscribe(
      (res: any) => {
        console.log(res);
        this.categoryData = res.categoryData;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addCategory() {
    const data: any = {};
    data.title = this.categoryForm.value.title;
    data.description = this.categoryForm.value.description;
    if (!this.editId) {
      this.home.addCategory(data).subscribe(
        (res) => {
          this.helper.showSuccess('Category Added Successfully.');
          this.getCategories();
          this.modalRef.hide();
        },
        (err) => console.log(err)
      );
    } else {
      data.id = this.editId;
      this.home.updateCategory(data).subscribe(
        (res) => {
          this.helper.showSuccess('Category updated successfully');
          this.getCategories();
          this.modalRef.hide();
        },
        (err) => console.log(err)
      );
    }
  }

  deleteCategory(id) {
    const data: any = {};
    data.id = id;
    this.home.deleteCategory(data).subscribe((res) => {
      this.helper.showSuccess('Category deleted successfully');
      this.getCategories();
    }, err => console.log(err));
  }

  logout() {
    this.helper.delAllPREF();
    this.router.navigate(['/login']);
  }

  openModal(template: TemplateRef<any>, id?, index?) {
    this.editId = id ? id : null;
    if (this.editId) {
      this.categoryForm.get('title').setValue(this.categoryData[index].title);
      this.categoryForm.get('description').setValue(this.categoryData[index].description);
    }
    this.modalRef = this.modalService.show(template);
  }
  closeModel() {
    this.editId = null;
    this.modalRef.hide();
  }
}
