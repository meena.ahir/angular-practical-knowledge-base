import { Injectable } from '@angular/core';
import { ApisService } from './apis.service';
import { HelperService } from './helper.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  token: any;
  constructor(public apiService: ApisService, private helper: HelperService) { }

  userSignup(data) {
    return this.apiService.postWithoutHeader('/user/sign-up', data);
  }

  userLogin(data) {
    return this.apiService.postWithoutHeader('/user/login', data);
  }

  addCategory(data) {
    return this.apiService.postWithHeader('/category', data, this.helper.getPREF('token'));
  }

  getCategories() {
    console.log(this.helper.getPREF('token'))
    return this.apiService.getWithHeader('/category', this.helper.getPREF('token'));
  }

  updateCategory(data) {
    return this.apiService.putWithHeader('/category/' + data.id, data ,this.helper.getPREF('token'))
  }

  deleteCategory(data) {
    return this.apiService.deleteWithHeader('/category/' + data.id, this.helper.getPREF('token'))
  }

  addContent(data) {
    return this.apiService.postWithHeader('/content', data, this.helper.getPREF('token'));
  }

  getContents(categoryId) {
    console.log(this.helper.getPREF('token'))
    return this.apiService.getWithHeader('/content/' + categoryId, this.helper.getPREF('token'));
  }
}
