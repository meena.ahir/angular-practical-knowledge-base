import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HelperService } from '../services/helper.service';
import { HomeService } from '../services/home.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements OnInit {
  contentData: any = [];
  contentForm: FormGroup;
  modalRef: BsModalRef;
  public categoryId = '';
  content: string;
  constructor(
    private home: HomeService,
    private modalService: BsModalService,
    public helper: HelperService,
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.categoryId = this.route.snapshot.params.id;
    this.getContents();
    this.contentForm = this.fb.group({
      title: [''],
      description: [''],
      image: ['']
    });
  }

  ngOnInit(): void {
  }

  getContents() {
    this.home.getContents(this.categoryId).subscribe(
      (res: any) => {
        console.log(res);
        this.contentData = res.contentData;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addContent() {
    const formData = new FormData();
    formData.append('categoryId', this.categoryId);
    formData.append('title', this.contentForm.get('title').value);
    formData.append('description', this.contentForm.get('description').value);
    formData.append('image', this.contentForm.get('image').value);
    this.home.addContent(formData).subscribe(
      (res) => {
        this.helper.showSuccess('Content Added Successfully.');
        this.getContents();
        this.modalRef.hide();
      },
      (err) => console.log(err)
    );
  }

  openModal(template: TemplateRef<any>, index?) {
    this.modalRef = this.modalService.show(template);
  }
  closeModel() {
    this.modalRef.hide();
  }

  handleFileInput(event) {
    this.contentForm.patchValue({
      image: event.target.files[0]
    });
  }

}
