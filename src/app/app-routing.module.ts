import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardNotLoggedInGuard } from './services/auth-guard-not-logged-in.guard';
import { AuthGuardLoggedIn } from './services/auth-guard.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'sign-up',
    pathMatch: 'full'
  }, {
    path: 'sign-up',
    loadChildren: () => import('./sign-up/sign-up.module').then(m => m.SignUpModule),
    canActivate: [AuthGuardNotLoggedInGuard]
  }, {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
    canActivate: [AuthGuardNotLoggedInGuard]
  }, {
    path: 'category',
    loadChildren: () => import('./category/category.module').then(m => m.CategoryModule),
    canActivate: [AuthGuardLoggedIn]
  },  {
    path: 'content/:id',
    loadChildren: () => import('./content/content.module').then(m => m.ContentModule),
    canActivate: [AuthGuardLoggedIn]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
